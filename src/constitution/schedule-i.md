# Schedule I: Members of the Board of Advisors

## Ordinary Members

- Avery McFadden
- Becca Reed
- Ben Battye
- Cal Parker
- Callum Warner
- Charlotte Lynn
- Cole Jowett
- Evan Kreutzwiser
- James Gullberg
- Kashan Rauf
- Kieran Green
- Koen Dyck
- Laura Cochrane
- Levi Guo
- Liam Doris
- Maxim Pletnev
- Michael Cassidy
- Mike Stefan
- Nick Mertin
- Nikola Nikolov
- Oliver Lynn
- Patrick Shu
- Peter Tennant
- Phie Truong
- Ryan Jacobson
- Shashank Ojha
- Taylor Hambleton
- Theo Lemay
- Will Steedman
- Xan Giuliani (Chair)

## *Ex Officio* Members (Non-Voting)

| Name                        | Position                                                    |
| --------------------------- | ----------------------------------------------------------- |
| Mo Kelley                   | Vice President (Student Affairs) of the Engineering Society |
| Tanner Warren               | Director of Design of the Engineering Society               |
| Prof. Keyvan Hashtrudi-Zaad | Faculty Advisor                                             |
| Cameron McRae               | Alumni Advisor                                              |
| Andrew Strauss              | Alumni Advisor                                              |
