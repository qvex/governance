# Schedule III: Working Groups

| Description | Members | Terms of Reference |
|-------------|---------|--------------------|
| Internal Processes Working Group | Nick Mertin (chair), Zane Al-Hamwy, Mike Stefan, Levi Guo, Patrick Shu, Cole Jowett, Xan Giuliani, and Avery McFadden | Revise team policies and NDA/IP contract for this season. |
| Competitions Working Group | Nick Mertin (chair), Charlotte Lynn, Mike Stefan, Oliver Lynn | See Appendix B section 13. |
