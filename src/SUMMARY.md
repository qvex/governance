# Summary

[Overview](README.md)
- [Team Constitution](constitution/README.md)
  - [Appendix A: Procedures for Board Meetings](constitution/appendix-a.md)
  - [Appendix B: Policy Manual](constitution/appendix-b.md)
  - [Schedule I: Members of the Board of Advisors](constitution/schedule-i.md)
  - [Schedule II: Executive Officers](constitution/schedule-ii.md)
  - [Schedule III: Working Groups](constitution/schedule-iii.md)
- [Team Contract](contract.md)
- [Board Agendas and Meeting Minutes](minutes.md)
<!-- - [Operating Budget](budget.md) -->
