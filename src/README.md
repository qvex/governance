# Overview

This book contains the up-to-date governing documents of Queen's VEX U Robotics
Team. It includes the following documents:

- The [Team Constitution](constitution/) is the foundational document of the
  team which sets out its overall structure, including the Board of Advisors.
  Subordinate documents are included as appendices or schedules to the
  constitution as appropriate:
  - [Appendix A](constitution/appendix-a.md) sets the procedures for meetings of
    the Board of Advisors.
  - [Appendix B](constitution/appendix-b.md) contains the team's policy manual.
  - [Schedule I](constitution/schedule-i.md) is the list of members and officers
    of the Board of Advisors.
  - [Schedule II](constitution/schedule-ii.md) is the list of team executive
    officers and mandate letters.
  - [Schedule III](constitution/schedule-iii.md) is the list of working groups.
- [Team Contract](contract.md) contains the contract signed by all team members
  governing their rights and responsibilities on the team.
- [Board Agendas and Meeting Minutes](minutes.md) contain summaries of reports
  presented and decisions made at each meeting of the Board of Advisors.
<!-- - [Operating Budget](budget.md) contains the budget allocations for the current
  season, as approved by the Board of Advisors, and the latest actuals reported
  to the Board. -->

## Summary

The team is overseen by a Board of Advisors, who have the ultimate authority to
make decisions for the team, and who appoint the President and Vice Presidents.
The Board meets regularly and all team members are welcome at meetings.

## Copyright

Copyright © 2024-2025 Queen's VEX U Robotics Team, an affiliated group of the
Engineering Society of Queen's University.
