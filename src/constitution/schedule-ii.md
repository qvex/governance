# Schedule II: Executive Officers

| Title | Name | Mandate |
| ----- | ---- | ------- |
| President | Patrick Shu | Be responsible for representing the team to EngSoc, Smith Engineering and the REC Foundation, drive the team’s overall direction, and oversee the day-to-day operations of the team in collaboration with the Vice Presidents. |
| Vice President (Business & EDII) | Phie Truong | Be responsible for the team’s relationships with sponsors and the community. Develop and maintain a sponsorship package and business plan, find and communicate with potential sponsors, and plan outreach events to increase the visibility of the team. |
| Vice President (Finance & Logistics) | Oliver Lynn | Be responsible for the team’s banking, budgeting, financial reporting, and travel. Coordinate with other Vice Presidents and Technical Leads to make sure that their areas have the funding they need, including managing the team’s revenue and expenditures. Ensure compliance with OCASP. |
| Vice President (Media) | Charlotte Lynn | Be responsible for all media projects on the team, including managing the team’s social media accounts, developing team merchandise, and developing videos and other media for recruitment, engagement and showcasing the team externally. |
| Vice President (Technical) | Cole Jowett | Be responsible for all technical activities on the team, including technical training. Supervise the technical directors and project leads. Ensure good project management of the technical activities and proper use of spaces used by the team. Ensure safety for all technical activities and spaces. |
