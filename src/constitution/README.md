# Team Constitution

Whereas Queen's VEX U Robotics Team ("the Team") is a ratified Design Team
affiliated with the Engineering Society of Queen's University ("EngSoc") for the
purpose of competing in the VEX U Robotics Competition, this document provides
for the structure of the team and is the source of authority for all
decision-making on the team.

## Article I: Objectives

1.  The objectives of the Team are:
    1.  To compete in the VEX U Robotics Competition on behalf of Queen's University.
    2.  To provide Queen's University students with an extracurricular
        opportunity to learn and develop their skills in areas relevant to
        robotics through participation in the competition.
2.  The activities of the Team shall be carried on with no intention of personal
    financial gain; all profits, grants, fees, and accretions shall be used
    uniquely for carrying out the team's objectives.
3.  Notwithstanding s. 2, the ownership of intellectual property developed
    through team activities shall belong to those who developed it, except as
    otherwise determined in accordance with this document.

## Article II: Affiliation with the Engineering Society

4.  As a ratified Design Team affiliated with EngSoc, the Team shall comply with
    the Constitution and By-Laws EngSoc and this document shall be subordinate
    to the same.

## Article III: Board of Advisors

5.  There shall exist a Board of Advisors which is ultimately responsible for
    the team and has complete authority to make decisions on behalf of the team,
    subject to the provisions of this document and, in accordance with s. 4, the
    governing documents of EngSoc.
6.  The Board of Advisors shall consist of an arbitrary number of ordinary
    (voting) members and the following *ex officio* non-voting members:
    1.  The Vice President (Student Affairs) of EngSoc;
    2.  The Director of Design of EngSoc;
    3.  The Faculty Advisor of the Team, appointed pursuant to s. 16; and
    4.  Alumni Advisors of the Team, appointed pursuant to s. 17.
7.  The Board of Advisors may from time to time appoint new ordinary members to
    itself from among the general members of the Team.
    1.  The Board of Advisors shall make an explicit effort to regularly find
        general members who have demonstrated trustworthiness and commitment to
        the Team's objectives and consider these members for appointment to the
        Board.
    2.  Ordinary members of the Board of Advisors shall automatically be removed
        upon their graduation or withdrawal from Queen's University.
    3.  No member may be assigned independent responsibility, managerial or
        otherwise, without being appointed to the Board of Advisors.
    4.  The intent of these provisions is to ensure that membership of the Board
        is reflective of the current active membership of the team, under the
        principle that if a member can be trusted with authority to competently
        complete a project independently, then the team would benefit from
        including that member on the Board.
8.  The Board of Advisors may remove an ordinary member from itself, with the
    support of at least two-thirds of all ordinary members of the Board.
9.  Quorum for meetings of the board shall be one third of the ordinary members,
    rounded up, and all decisions shall be made by simple majority unless
    otherwise specified.
10. Any member of the Team may attend meetings of the Board of Advisors and
    contribute to discussions.
    1.  The Board of Advisors may enter a closed session in order to discuss
        sensitive matters, at its own discretion.
11. Appendix A to this document shall provide the procedures for meetings of the
    Board of Advisors.
    1.  In case of any conflict, this document shall take precedence over
        Appendix A.
12. Schedule I to this document shall enumerate the members of the Board of
    Advisors.

## Article IV: Executive Officers

13. The Board of Advisors shall annually elect the President of the Team and
    oversee the appointment of a number of Vice Presidents of the Team, to
    collectively serve as the Team's Executive Officers.
14. The Board shall provide a brief mandate letter for each Executive Officer
    which describes:
    1.  The purpose and responsibilities of that Executive Officer; and
    2.  The authority delegated to that Executive Officer.
15. Schedule II to this document shall enumerate the Executive Officers of the
    Team, including titles and mandate letters.

## Article V: Advisors

16. The Team shall have a Faculty Advisor, who shall be a member of the Stephen
    J. R. Smith Faculty of Engineering and Applied Science, and shall be chosen
    or replaced by the Board of Advisors, with the consent of the Vice
    President (Student Affairs) of EngSoc.
17. The Team may have any number of Alumni Advisors, who shall be former members
    of the Team and may be appointed or removed by the Board of Advisors.

## Article VI: Membership

18. Any member of the Alma Mater Society of Queen's University ("the AMS"), the
    Society of Graduate and Professional Students of Queen's University ("the
    SGPS"), or any faculty society thereof, may sign up as a member of the Team
    by following a process set by the Board of Advisors.
    1.  All sign-ups shall be accepted, subject to s. 19.
    2.  There shall be no fee for joining the team.
    3.  Notwithstanding s. 16.b, there may be a fee associated with travel on
        the team, subject to the direction of the Board of Advisors.
19. The Board of Advisors may permanently remove any member of the Team for
    inappropriate behaviour, with the support of at least two-thirds of all
    ordinary members of the Board.

## Article VII: Policies and Agreements

20. Appendix B to this document shall contain the Team Policy Manual.
    1.  The Policy Manual may create additional positions within the team and
        specify delegations of authority to them; anyone who holds such
        positions shall be a member of the Board of Advisors.
    2.  The Policy Manual shall set out procedures and requirements for
        financial management of the Team.
    3.  In case of any conflict, this document shall take precedence over
        Appendix B.
21. The Team may create agreements and require members to sign in order to
    participate in Team projects, in order to safeguard the intellectual
    property of the Team, other members, and third parties.
    1.  While these agreements may be updated from time to time, the Team
        recognizes that it cannot unilaterally change the legal obligations
        agreed to by members who have already signed an agreement.
    2.  The procedures and requirements for the agreements shall be set out in
        the Policy Manual.

## Article VIII: Working Groups

22. The Board of Advisors may from time to time appoint Working Groups to
    fulfill particular responsibilities.
23. Schedule III to this document shall enumerate the active Working Groups,
    including for each one:
    1.  A description;
    2.  The members and chair(s) of the working group; and
    3.  The terms of reference of the working group, including its scope,
        timelines, and any delegated authority.

## Article IX: Amendments

27. This document may be amended by the Board of Advisors, with the consent of
    the Vice President (Student Affairs) of EngSoc and the support of at least
    two-thirds of all ordinary members of the board.
    1.  Notice of a motion to amend this document must be given at the prior
        meeting of the Board of Advisors.
28. The appendices to this document may be amended at any time by the Board of
    Advisors.
29. The schedules to this document may be altered as a consequence of decisions
    taken by the Board of Advisors or delegated authorities, and shall be kept
    up to date by the President.
