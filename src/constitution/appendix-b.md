# Appendix B: Policy Manual

## Part I: Team Finances and Purchases

1.  The budget shall be drafted annually by the Vice President (Finance &
    Logistics) using the EngSoc budget template and submitted to the Board of
    Advisors for approval.
    1.  Changes to the budget allocation beyond the included contingency must be
        approved by the Board of Advisors.
    2.  The team shall aim to break even each season and shall make an effort to
        maintain an account balance of at least $2500 to support necessary
        purchases.
2.  The Vice President (Finance & Logistics) shall present an update at each
    meeting of the Board of Advisors which includes:
    1.  New actual expenditures since the last report
    2.  New actual revenues since the last report
    3.  Changes to projected finances for the remainder of the season
    4.  Remaining budget in each expense line
3.  Only Executive Officers and Directors may authorize expenses, and only
    within budget lines under their portfolio.
    1.  Expenses over $200 must additionally be authorized by the Vice President
        (Finance & Logistics) to ensure they are reasonable.
    1.  The Vice President (Finance & Logistics) shall maintain a shared
        spreadsheet of transactions and actuals for each budget line.
4.  Packages ordered for the Team shall be tracked in a shared spreadsheet
    maintained by the Vice President (Finance & Logistics).
    1.  Packages shall be shipped to one of:
        1.  The Engineering Student Lounge (Beamish-Munro Hall room 106)
        2.  A pickup location (e.g., post office, UPS store, etc.)
    2.  Exceptions to the above requirement may be granted by the Vice President
        (Finance & Logistics) where it is not feasible.
    3.  Where permitted by the supplier, packages shall include a Purchase Order
        number, which is also in the spreadsheet, using the format `LLLLAANN`.
        1.  `LLLL` is the budget line number.
        2.  `AA` is the first and last initial of the person who authorized the
            purchase.
        3.  `NN` is a 2-digit number which makes this PO unique within the
            current season.

## Part II: Logistics

5.  Travel plans for competitions shall be subject to approval for the Board of
    Advisors, including:
    1.  Projected transportation schedule
    2.  Projected cost
    3.  Fee structure for participating Team members
    4.  List of participating Team members and allocation of responsibilities
6.  The Vice President (Finance & Logistics) shall ensure that team members sign
    the Team Contract as a prerequisite to participating in team activities each
    season.

## Part III: Technical Team Structure

7.  The technical team shall be led by the following Directors, who report to
    the Vice President (Technical):
    1.  Two Directors of Mechanical
    2.  One Director of Software
    3.  One Director of Electrical
8.  Additional Project Leads may be hired to report to these directors.
9.  Only Project Leads, Directors, and the Vice President (Technical) may be
    made responsible for technical work, including projects and training.
    1.  General members may work with one or more Project Leads or Directors on
        their respective projects, but shall not be accountable to the team for
        its completion.
    2.  A Director or the Vice President (Technical) may, with their consent,
        designate a general member as an Acting Project Lead to cover for the
        unavailability of a Project Lead or Director.

## Part IV: Media Team Structure

10. The media team shall be led by the Vice President (Media).
11. Additional Media Project Leads may be hired to report to the Vice President
    (Media).
12. Only Media Project Leads and the Vice President (Media) may be made
    responsible for media work, including projects and training; general members
    may work with on media projects, but shall not be accountable to the team
    for their completion.

## Part V: Tryouts and Competitions

13. The Board of Advisors shall appoint a Competitions Working Group (CWG) of at
    least 3 memmbers, with the following responsibilities.
    1.  Run tryouts for drivers and drive coaches during the fall semester.
    2.  For each competition which the team is planning to attend, with
        budgetary input from the Vice President (Finance & Logistics), review
        applications for team-subsidized competition travel (if applicable) from
        team members.
14. Tryouts shall be conducted in two rounds as follows.
    1.  All team members shall be permitted, in the first round, to try out as a
        driver or as a coach, or both, regardless of prior experience.
    2.  Team members may choose to try out as a driver/coach pair or
        individually as a driver or coach (or both); in the latter case, they
        shall be paired up with other members trying out, at the discretion of
        the CWG.
    3.  The CWG shall, using the official field and one or more robots provided
        by the team, simulate a competition environment to determine each
        candidate's ability to drive or coach under stress.
    4.  From the first round, the CWG shall select a roster of drivers and
        coaches who have promising abilities; the composition of this roster is
        at the discretion of the CWG but should ideally be around 4 each.
    5.  The CWG shall, if they deem necessary, run a second round of tryouts,
        limited to those selected onto the roster in the first round, to
        determine optimal rankings and pariings.
15. Competition responsibilities and subsidies shall be determined as follows.
    1.  All team members shall be permitted to apply to travel to the
        competition under subsidy from the team.
    2.  For a competition which requires travel, the Vice President (Finance &
        Logistics) shall provide the CWG with budgetary estimates necessary to
        determine the amount of travel subsity which can be provided.
    3.  The CWG shall review all applications and produce a recommended roster
        for competition attendance, with at least a primary and backup member
        assigned to each of the following key positions:
        1.  Robot A driver (member of drive team)
        2.  Robot A coach (member of drive team)
        3.  Robot B driver (member of drive team)
        4.  Robot B coach (member of drive team)
        5.  Head strategist (member of drive team)
        6.  Head scout (responsible for personnel management of the scouting team)
        7.  Head of pit crew (responsible for personnel management of the pit crew)
    4.  The CWG may deviate from the above recommendations at their discretion
        depending on the size of the competition and budget available.
    5.  The Vice President (Finance & Logistics) and the  CWG shall submit their
        final recommendations for competition budget, logistics and responsibilities
        to the Board of Advisors, who shall have the final decision.

## Part VI: Business

16. The Vice President (Business & EDII) shall develop a Sponsorship Package
    outlining the team's mission and benefits available to sponsors, which must
    be approved by the Board of Advisors.
    1.  The Vice President (Business & EDII), with the agreement of the
        President, may enter the team into a sponsorship agreement in accordance
        with the approved Sponsorship Package, subject to the following
        restriction.
    2.  An agreement for a Title Sponsor must be approved by the Board of
        Advisors.

## Part VII: Queen's Robotics Cup

17. Queen's Robotics Cup (QRC) shall function as a subsidiary organization under
    the supervision of the Board of Advisors, with a mandate to run at least one
    official VEX U tournament each year, in coordination with the Robotics
    Education and Competition Foundation.
18. QRC shall be led a Co-Chair (Competition) and Co-Chair (Administration), who
    report directly to the Board of Advisors independently from the Executive
    Officers of QVEX, and the following Directors who report to them:
    1.  Two Directors of Logistics, responsible for venues, equipment rentals,
        and hospitality.
    2.  One Director of Finance, responsible for the budget/financial account
        and processing all revenues and expenses.
    3.  One Director of Information Technology, responsible for all technical
        equipment necessary to run the competition.
    4.  One Director of Communications, responsible for QRC social media and
        marketing to potential attending teams.
19. QRC shall have a separate budget/financial account from QVEX, with the
    following policies.
    1.  The QRC budget shall be presented and approved annually by the Board of
        Advisors.
    2.  QVEX may, at the discretion of the Board of Advisors, provide funding to
        QRC in support of its operations; this must be reflected in the budget
        and actuals of each account.
    3.  QRC shall ensure the Board of Advisors is kept updated with the status
        of competition planning and any issues relevant to the events being run.
20. QRC may hire additional positions reporting to the above Directors as they
    see fit.
21. The Board of Advisors shall make reasonable efforts to ensure the QRC
    executive for the new year, or at a minimum new Co-Chairs, are appointed
    prior to the main event in the previous year.
