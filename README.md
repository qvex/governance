# QVEX Governance

This book contains the up-to-date governing documents of Queen's VEX U Robotics
Team. It can be most easily viewed [online](https://governance.qvex.ca/).

## Copyright

Copyright © 2024-2025 Queen's VEX U Robotics Team, an affiliated group of the
Engineering Society of Queen's University.
