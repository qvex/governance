# Board Agendas and Meeting Minutes

Agendas and minutes for meetings of the Board of Advisors are posted
[here](https://engsoc-my.sharepoint.com/:f:/g/personal/qvex_engsoc_queensu_ca/Etvf73YkbpdAttuQDBISZcUBMhWlHCUMDb_kG4xPmpMclw?e=yzm3GC).
