# Appendix A: Procedures for Board Meetings

## Part I: Scheduling of Meetings

1.  A meeting of the Board of Advisors may be called at any time by the
    Chair (as defined below) or any Executive Officer.
    1.  Notice of the time and location of the meeting must be provided to the
        members of the Board at least 24 hours in advance.
    2.  A meeting may take place in person or online; however, Team members not
        able to meet in person shall be permitted to participate in meetings
        remotely, regardless of the meeting format.
2.  The Board shall meet at least once every 15 business days, as defined by
    Queen's University, except during the months of May through August.

## Part II: Order of Business

3.  The Board of Advisors shall have a Chair annd Vice-Chair, as follows.
    1.  These shall be elected by the Board of Advisors by the process set out
        in Part III and shall be ordinary members of the Board.
    2.  Should the Chair become vacant, the Vice-Chair automatically becomes the
        new Chair, and a new Vice-Chair must be elected.
    3.  Neither the Chair nor the Vice-Chair shall be an Executive Officer.
    4.  All meetings of the Board of Advisors shall be chaired by the Chair or
        Vice-Chair.
    5.  If both positions are vacant, a meeting to elect a new Chair may be
        chaired by any Executive Officer or, with the support of at least
        two-thirds of the present ordinary members of the Board, any other
        person.
4.  The order of business for meetings of the Board of Advisors shall be:
    1.  Adoption of the agenda
    2.  Approval of the minutes of the previous meeting
    3.  Presentations
    5.  Discussions and decisions
    6.  Reports from the Executive Officers
    7.  Reports from the Working Groups
    8.  Statements and questions by members
5.  Making and distribution of the agenda and minutes:
    1.  The agenda shall follow the format of the order of business in s. 4.
    2.  Any presentation, motion or discussion topic submitted to the Chair
        at least 8 hours before the meeting start time with the support of a
        member of the Board shall be included in the agenda.
    3.  The agenda shall be distributed to members of the Board no later than 1
        hour before the meeting start time.
    4.  The Chair or a designate shall take minutes of the meeting, which shall
        include at a minimum a summary of the points of presentations and
        reports and the results of all decisions taken.
    5.  Minutes shall be distributed to Board members no later than 1 week after
        the conclusion of the meeting.
6.  If the person responsible for a report is not able to be present for a
    meeting, they shall ensure that a different Team member is able to give the
    update on their behalf.

## Part III: Elections

7.  Elections of the Executive Officers and of any other position that the Board
    wishes to elect shall be conducted in accordance with these procedures.
8.  All candidates in an election and every person who is not a member of the
    Board shall leave the room during debate and voting.
9.  Unless two or more ordinary members of the Board request that the election
    be conducted by secret ballot, all elections shall be conducted by runoff
    voting by show of hands as follows:
    1.  The Chair shall read the names of the candidates in alphabetical order
        by last name.
    2.  After each name, voting Board members who agree with the election of
        that candidate shall raise their hand, and the Chair shall record the
        number of votes; each member may only vote for one candidate in each
        round, but may abstain.
    3.  At the end of the round, if one candidate received the majority of the
        votes, then that candidate is the winner; otherwise, the candidate with
        the fewest votes, and all candidates who received fewer than 20% of the
        votes, are removed from the list, and the process is repeated until a
        winner is determined.
10. If two or more ordinary members of the Board request than the election be
    conducted by secret ballot, then it shall proceed by instant runoff voting
    as follows:
    1.  All voting Board members shall rank their preferred candidates on paper
        and submit the paper to the Chair.
    2.  The Chair shall determine the winner (and if necessary, the runner-up)
        in the same process as in s. 14, using the highest-ranked candidate not
        yet eliminated from each ballot.
11. If there is only one candidate in the election, it shall be conducted by a
    confidence vote instead, either by show of hands or secret ballot.
12. The results of the election shall be immediately reported to the board and
    shall not require further approval or ratification.

## Part IV: Rules of Procedure
13. For all substantive decisions other than elections, the Board of Advisors
    shall make a decision by way of a substantive motion, which shall precisely
    state the order to be made by the Board under its authority.
    2.  Any substantive motion may be amended by the consent of the mover or the
        Board.
    3.  Decisions made by substantive motions shall take effect immediately,
        unless the motion states otherwise.
14. Any motion, including procedural motions relating to the current meeting,
    shall be supported by at least one member of the Board of Advisors in order
    to be considered.
15. Decisions on motions may be taken by unanimous consent or a show of hands
    1.  Any member of the Board of Advisors may request that their opposition or
        abstention on a motion be recorded in the minutes.
14. The Chair shall ensure that any member of the Board of Advisors participating
    remotely may vote by an appropriate electronic analogue to the methods
    described in Part III or Part IV.
15. Where the Chair deems necessary, a substantive motion may be made without a
    meeting, as follows.
    1.  The Chair shall share the text of the motion electronically with all
        members of the Board of Advisors and request their vote, specifying a
        time limit.
    2.  For the purposes of counting votes, all ordinary members of the Board of
        Advisors shall be assumed to oppose the motion unless they vote
        otherwise.
    3.  The Chair may stop counting once enough votes have been received to make
        only one outcome possible.
